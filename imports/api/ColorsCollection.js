import { Mongo } from 'meteor/mongo';

export const ColorsCollection = new Mongo.Collection('colors');
