import { Mongo } from 'meteor/mongo';

export const KeycapsCollection = new Mongo.Collection('keycaps');
