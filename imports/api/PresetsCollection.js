import { Mongo } from 'meteor/mongo';

export const PresetsCollection = new Mongo.Collection('presets');
