import { Mongo } from 'meteor/mongo';

export const SwitchesCollection = new Mongo.Collection('switches');
