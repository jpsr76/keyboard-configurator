import { Mongo } from 'meteor/mongo';

export const DiscountCodesCollection = new Mongo.Collection('discountcodes');
