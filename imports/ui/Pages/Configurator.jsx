import React from 'react';
import ConfigProvider from '../Components/Configurator/ConfigProvider';
import ConfigLayout from '../Components/Configurator/ConfigLayout';

export const Configurator = () => {

  return (
    <>
      <ConfigProvider>
        <ConfigLayout />
      </ConfigProvider>
    </>
  );
};
