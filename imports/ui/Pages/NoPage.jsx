import React from 'react';

import { useNavigate } from 'react-router-dom';

import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

export const NoPage = () => {

    const navigate = useNavigate();

    return (
        <Typography variant="h1" color="inherit" noWrap>
        <Link 
          component="button"
          onClick={() => {
            navigate('/');
          }}>
          ERROR: 404 Page not Found
        </Link>{' '}
      </Typography>
    );
};