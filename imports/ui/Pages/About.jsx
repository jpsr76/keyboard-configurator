import React from 'react';

import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export const About = () => {

    return (
        <Container component="main" maxWidth="xs">
            <Box
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}
            >
                <Typography component="h1" variant="h4" align='center'>
                    Ein THM Webframeworks Projekt
                </Typography>
                <Typography gutterBottom variant="section" component="div">
                    Diese Anwendung wurde 2024 als Teil des Moduls Webframeworks an der THM erstellt.
                </Typography>
                <Typography component="h1" variant="h6" align='center'>
                    Die Entwickler
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Till-Leon Peschel
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Jan Straßburg
                </Typography>
                
            </Box>
        </Container>
    );
};