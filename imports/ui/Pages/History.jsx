import { Meteor } from 'meteor/meteor';
import React, { useState, Fragment } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { OrdersCollection } from '/imports/api/OrdersCollection';

import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import HistoryIcon from '@mui/icons-material/History';
import List from '@mui/material/List';

import { useAuth } from '../Components/Authentication/AuthContext';
import { Order } from '../Components/History/Order';

export const History = () => {

    const { user } = useAuth();

    const userFilter = user ? { userId: user._id } : {};

    const orders = useTracker(() =>
      OrdersCollection.find(userFilter, {
        sort: { createdAt: -1 },
      }).fetch()
    );

    
return (
  <Container component="main" maxWidth="md">
  <Box
    sx={{
      marginTop: 8,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }}
  >
    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
      <HistoryIcon fontSize='large' />
    </Avatar>
    <Typography component="h1" variant="h5">
      Order History
    </Typography>
  </Box>
  <Box component=""sx={{ mt: 1 }}>
    <List>
      { orders.map(order => <Order key={ order._id } order={ order }/>) }
    </List>
  </Box>
</Container>
  );
};