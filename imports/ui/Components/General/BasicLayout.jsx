import React from 'react';
import Header from './Header';
import Footer from './Footer';

import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

const defaultTheme = createTheme();

const BasicLayout = ({ children }) => {
  return (
    <>
    <ThemeProvider theme={defaultTheme}>
    <CssBaseline />
      <Header />
      {children}
      <Footer />
    </ThemeProvider>
    </>
  );
}

export default BasicLayout;