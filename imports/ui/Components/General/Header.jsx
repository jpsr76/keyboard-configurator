import React from 'react';

import { useNavigate } from 'react-router-dom';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import SettingsIcon from '@mui/icons-material/Settings';

import HeaderMenu from './HeaderMenu';

const Header = () => {

  const navigate = useNavigate();

  return (
    <AppBar
    position="absolute"
    color="default"
    elevation={0}
    sx={{
      position: 'relative',
      borderBottom: (t) => `1px solid ${t.palette.divider}`,
    }}
  >
    <Toolbar sx={{ display: 'flex', justifyContent: 'space-between', textAlign: 'center' }}>
      <Typography variant="h6" color="inherit" noWrap>
        <Link 
          color="inherit"
          component="button"
          onClick={() => {
            navigate('/');
          }}>
          <SettingsIcon fontSize="medium"/>
          Keyboard Configurator
        </Link>{' '}
      </Typography>
      <HeaderMenu />
    </Toolbar>
  </AppBar>
  );
}

export default Header;