import * as React from 'react';

import { Meteor } from 'meteor/meteor';
import { useNavigate } from 'react-router-dom';

import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import History from '@mui/icons-material/History';
import Logout from '@mui/icons-material/Logout';
import Login from '@mui/icons-material/Login';

import { useAuth } from '../Authentication/AuthContext';

export default function HeaderMenu() {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const navigate = useNavigate();
  const { user, onLogout } = useAuth();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    console.log('click: ' + event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleHistoryButton = () => {
    handleClose
    navigate(to="/history");
  }

  // Maybe fix login navigation to store location
  const handleLoginButton = () => {
    handleClose
    navigate(to="/login");
  }

  const handleLogoutButton = () => {
    handleClose
    Meteor.logout(onLogout);
  }


  return (
    <React.Fragment>
      <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
        <Tooltip title="Header Menu">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? 'header-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
          >
            <Avatar sx={{ width: 32, height: 32 }}>M</Avatar>
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="header-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&::before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem onClick={handleClose}>
          <Avatar /> {user ? (user.username) : ('Guest')}
        </MenuItem>
        <Divider />
        { user && (
          <MenuItem onClick={handleHistoryButton}> 
            <ListItemIcon>
              <History fontSize="small" />
            </ListItemIcon>
            Order History
          </MenuItem>
        )}
        { user ? (
          <MenuItem onClick={handleLogoutButton}>
            <ListItemIcon>
              <Logout fontSize="small" />
            </ListItemIcon>
            Logout
          </MenuItem>
        ) : (
          <MenuItem onClick={handleLoginButton}>
            <ListItemIcon>
              <Login fontSize="small" />
            </ListItemIcon>
            Login
          </MenuItem>
        )}
      </Menu>
    </React.Fragment>
  );
}