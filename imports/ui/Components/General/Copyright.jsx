import React from 'react';

import { useNavigate } from 'react-router-dom';

import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

export const Copyright = (props) => {
    const navigate = useNavigate();

    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
          {'Copyright © '}
          <Link 
            color="inherit"
            component="button"
            variant="body2"
            onClick={() => {
              navigate('/about');
            }}>
            Configurator
          </Link>{' '}
          {new Date().getFullYear()}
          {'.'}
        </Typography>
      );
};