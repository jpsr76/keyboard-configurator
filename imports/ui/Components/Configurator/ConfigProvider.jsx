import React, { useState }  from 'react';

import { ConfigContext } from './ConfigContext';

const ConfigProvider = ({ children }) => {

    // Declaring usestates
    const [selectedCategory, setCategory] = useState(0);

    // Check if Preset is needed, depending on intensity of API calls in the preset Fragment in the Selector
    const [selectedPreset, setPreset] = useState(null);
    const [selectedBase, setBase] = useState(null);
    const [selectedSwitch, setSwitch] = useState(null);
    const [selectedKeycap, setKeycap] = useState(null);
    const [selectedColor, setColor] = useState(null);

    const value = {
      selectedCategory,
      setCategory,
      selectedPreset,
      setPreset,
      selectedBase,
      setBase,
      selectedSwitch,
      setSwitch,
      selectedKeycap,
      setKeycap,
      selectedColor,
      setColor,
    };

    return (
      <ConfigContext.Provider value={value}>
        {children}
      </ConfigContext.Provider>
    );
};

export default ConfigProvider;