import React from 'react';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';

import BlockIcon from '@mui/icons-material/Block';

import ButtonBase from '@mui/material/ButtonBase';

import { useConfig } from '../ConfigContext';

export const ColorItem = ({ color }) => {

    const { selectedColor, setColor } = useConfig();

    const handleElementClick = (color) => {
        if (selectedColor != null && selectedColor._id == color._id) {
            setColor(null);
        } else {
            setColor(color);
        }
      }

    return (
        <Grid item xs={4}>
            <ButtonBase onClick={(() => handleElementClick(color))} sx={{ width: '100%', height: '100%' }}>
                <Paper
                sx={{
                    p: 2,
                    margin: 'auto',
                    flexGrow: 1,
                    backgroundColor: (theme) =>
                    theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    height: '100%'
                }}
                >
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <BlockIcon fontSize='large' />
                </Avatar>
                <Typography component="h1" variant="h6" align='center'>
                    {color.name} 
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Price: ${color.price}
                </Typography>
                </Paper>
            </ButtonBase>
        </Grid>
    );
};