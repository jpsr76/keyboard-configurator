import React from 'react';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';

import BlockIcon from '@mui/icons-material/Block';

import ButtonBase from '@mui/material/ButtonBase';

import { useConfig } from '../ConfigContext';

export const KeycapItem = ({ keycap }) => {

    const { selectedKeycap, setKeycap } = useConfig();

    const handleElementClick = (keycap) => {
        if (selectedKeycap != null && selectedKeycap._id == keycap._id) {
            setKeycap(null);
        } else {
            setKeycap(keycap);
        }
      }

    return (
        <Grid item xs={6}>
            <ButtonBase onClick={(() => handleElementClick(keycap))} sx={{ width: '100%', height: '100%' }}>
                <Paper
                sx={{
                    p: 2,
                    margin: 'auto',
                    flexGrow: 1,
                    backgroundColor: (theme) =>
                    theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    height: '100%'
                }}
                >
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <BlockIcon fontSize='large' />
                </Avatar>
                <Typography component="h1" variant="h6" align='center'>
                    {keycap.name} 
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Price: ${keycap.price}
                </Typography>
                </Paper>
            </ButtonBase>
        </Grid>
    );
    
};