import React from 'react';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';

import BlockIcon from '@mui/icons-material/Block';

import ButtonBase from '@mui/material/ButtonBase';

import { useConfig } from '../ConfigContext';

export const PresetItem = ({ preset, base, switc, keycap, color }) => {

    const { 
        // selectedPreset, setPreset, 
        selectedBase, setBase, 
        selectedSwitch, setSwitch, 
        selectedKeycap, setKeycap, 
        selectedColor, setColor 
    } = useConfig();

    const handleElementClick = (preset) => {

        if ((
            // selectedPreset != null &&
            selectedBase != null &&
            selectedSwitch != null &&
            selectedKeycap != null &&
            selectedColor != null
        ) && (
            // selectedPreset._id == preset._id &&
            selectedBase._id == base._id &&
            selectedSwitch._id == switc._id &&
            selectedKeycap._id == keycap._id &&
            selectedColor._id == color._id
        )) {
            // setPreset(null);
            setBase(null);
            setSwitch(null);
            setKeycap(null);
            setColor(null);
        } else {
            // setPreset(preset);
            setBase(base);
            setSwitch(switc);
            setKeycap(keycap);
            setColor(color);
        }
      }

    return (
        <Grid item xs={12}>
            <ButtonBase onClick={(() => handleElementClick(preset))} sx={{ width: '100%', height: '100%' }}>
                <Paper
                sx={{
                    p: 2,
                    margin: 'auto',
                    flexGrow: 1,
                    backgroundColor: (theme) =>
                    theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    height: '100%'
                }}
                >
                <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                    <BlockIcon fontSize='large' />
                </Avatar>
                <Typography component="h1" variant="h6" align='center'>
                    {preset.name} 
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Description: {preset.description}
                </Typography>
                </Paper>
            </ButtonBase>
        </Grid>
    );
};