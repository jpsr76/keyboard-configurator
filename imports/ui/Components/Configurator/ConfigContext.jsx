import React from 'react';

export const ConfigContext = React.createContext(null);

export const useConfig = () => {
    return React.useContext(ConfigContext);
  };