import React from 'react';
import HeaderBar from './ConfigHeaderBar';
import Selector from './Selector';
import Preview from './Preview';
import Summary from './Summary';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';

import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const ConfigLayout = () => {
  
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));

  return (
    <Box sx={{ flexGrow: 1}}>
      <Grid container columnSpacing={4} rowSpacing={2} columns={12} justifyContent={'space-around'}>
        <Grid item xs={12}>
            <HeaderBar />
        </Grid>
        <Grid item xs={matches ? 3 : 12} >
            <Selector />
        </Grid>
        <Grid item xs={matches ? 4 : 12}>
            <Preview />
        </Grid>
        <Grid item xs={matches ? 3 : 12}>
            <Summary />
        </Grid>
      </Grid>
    </Box>
  );
}

export default ConfigLayout;