import * as React from "react"

const BaseSvgComponent = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 1080" {...props}>
    <path
      d="m107.303 725.933 1.857-3.447 2.916-2.21 951.049-405.463h2.438L1360.5 434.25l4.25 4-.75 2.75-981.75 548-18.25-8-254.25-246.25Z"
      style={{
        display: "inline",
        fill: "#666",
        strokeWidth: 0,
      }}
    />
    <path
      d="m120 723 260.247 245.736 3.358 1.415 3.36.176 3.712-1.237 964.051-531.656.354-1.238-.53-1.414-289.207-117.026-1.856-.265-1.857.354-940.584 402.697-1.06 1.016z"
      style={{
        fill: "#b8b8b8",
        strokeWidth: 0,
        fillOpacity: 1,
      }}
    />
    <path
      d="m384.846 982.383 979.966-543.758-4.062 26.063-973.563 552.937Z"
      style={{
        fill: "#4d4d4d",
        strokeWidth: 0,
      }}
    />
    <path
      d="m379.726 1018.688 7.566-1.022-1.5-35.792-10.887 1.017z"
      style={{
        fill: "#666",
        strokeWidth: 0,
      }}
    />
    <path
      d="m107.278 725.492 4.546 24.546 14.546 44.548 253.378 224.125-4.274-35.934z"
      style={{
        fill: "gray",
        strokeWidth: 0,
      }}
    />
  </svg>
)
export default BaseSvgComponent