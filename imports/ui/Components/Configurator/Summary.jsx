import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { useNavigate } from 'react-router-dom';

import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import { Divider } from '@mui/material';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

import { useConfig } from './ConfigContext';
import { useAuth } from '../Authentication/AuthContext';
import { DiscountCodesCollection } from '/imports/api/DiscountCodesCollection';
import { OrdersCollection } from '/imports/api/OrdersCollection';


const Summary = () => {

  const { selectedBase, selectedSwitch, selectedKeycap, selectedColor } = useConfig();
  const { user } = useAuth();
  const navigate = useNavigate();

  const [activeDiscount, setActiveDiscount] = React.useState(0);


  const price = 
  (selectedBase ? (selectedBase.price) : (0)) +
  (selectedSwitch ? (selectedSwitch.price) : (0)) +
  (selectedKeycap ? (selectedKeycap.price) : (0)) +
  (selectedColor ? (selectedColor.price) : (0));

  const totalPrice = (price * (100 - activeDiscount)) / 100; 

  const buttonEnabled = (selectedBase && selectedSwitch && selectedKeycap && selectedKeycap)

  const discountCodes = useTracker(() =>
  DiscountCodesCollection.find({}).fetch()
  );


  const checkDiscount = (e) => {
    found = discountCodes.find((element) => element.name.valueOf() === e.target.value.valueOf());
    if (found) {
      setActiveDiscount(found.percent);
    } else {
      setActiveDiscount(0);
    }
  };

  const placeOrder = (e) => {
    e.preventDefault();

    if (!selectedBase) return;
    if (!selectedSwitch) return;
    if (!selectedKeycap) return;
    if (!selectedColor) return;
 
    if (user) {
      OrdersCollection.insert({
        name: user.username + "'s Order",
        description: "Kurzbeschreibung",
        isPreset: false,
        base: selectedBase.name,
        switch: selectedSwitch.name,
        keycaps: selectedKeycap.name,
        color: selectedColor.name,
        discount: activeDiscount + "%",
        price: totalPrice,
        userId: user._id,
        createdAt: new Date(),
      });
      navigate('/history');
    } else {
      OrdersCollection.insert({
        name: "Guest Order",
        description: "Kurzbeschreibung",
        isPreset: false,
        base: selectedBase.name,
        switch: selectedSwitch.name,
        keycaps: selectedKeycap.name,
        color: selectedColor.name,
        discount: activeDiscount + "%",
        price: totalPrice,
        userId: null,
        createdAt: new Date(),
      });
      navigate('/');
    }

  };

  return (
    <Container component="main">
        <Paper
        sx={{
            p: 2,
            margin: 'auto',
            flexGrow: 1,
            backgroundColor: (theme) =>
            theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
        }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              width: '100%',
              height: '100%',
            }}
          >
            <Typography gutterBottom variant="h6" component="div">
                Summary
            </Typography>
            <Box sx={{p: 1}}>
              Base: {selectedBase ? (selectedBase.name) : ('None')}
            </Box>
            <Box sx={{p: 1}}>
              Switch: {selectedSwitch ? (selectedSwitch.name) : ('None')}
            </Box>
            <Box sx={{p: 1}}>
              Keycap: {selectedKeycap ? (selectedKeycap.name) : ('None')}
            </Box>
            <Box sx={{p: 1}}>
              Color: {selectedColor ? (selectedColor.name) : ('None')}
            </Box>
            <Divider flexItem/>
            <Box sx={{p: 2}}>
            <TextField onChange={checkDiscount} id="outlined-basic" margin='normal' label="Discount Codes" variant="outlined" />
            </Box>
            <Divider flexItem/>
            <Box sx={{p: 2}}>
              Total: {totalPrice} {activeDiscount != 0? ' (' + activeDiscount + '% Rabbatt)' : ''}
            </Box>
            <Box>
              <Button onClick={placeOrder} disabled={!buttonEnabled} variant="contained" endIcon={<SendIcon />}>
                Order
              </Button>
            </Box>
          </Box>
        </Paper>
    </Container>
  );
}

export default Summary;