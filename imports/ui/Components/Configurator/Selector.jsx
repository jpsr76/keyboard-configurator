import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import Grid from '@mui/material/Grid';

import { useConfig } from './ConfigContext';
import { Container } from '@mui/material';
import { PresetItem } from './Selector/PresetItem.jsx';
import { BaseItem } from './Selector/BaseItem.jsx';
import { SwitchItem } from './Selector/SwitchItem.jsx';
import { KeycapItem } from './Selector/KeycapItem.jsx';
import { ColorItem } from './Selector/ColorItem.jsx';

import { PresetsCollection } from '/imports/api/PresetsCollection';
import { BasesCollection } from '/imports/api/BasesCollection';
import { SwitchesCollection } from '/imports/api/SwitchesCollection';
import { KeycapsCollection } from '/imports/api/KeycapsCollection';
import { ColorsCollection } from '/imports/api/ColorsCollection';

const Selector = () => {

  const { selectedCategory } = useConfig();

  const presets = useTracker(() =>
    PresetsCollection.find({}).fetch()
  );

  const bases = useTracker(() =>
    BasesCollection.find({}).fetch()
  );

  const switches = useTracker(() =>
    SwitchesCollection.find({}).fetch()
  );

  const keycaps = useTracker(() =>
    KeycapsCollection.find({}).fetch()
  );

  const colors = useTracker(() =>
    ColorsCollection.find({}).fetch()
  );

  const findCollectionElementByName = (collection, name) => {
    return collection.find((e) => {
      return e.name == name;
    })
  }

  return (
    <Container component="main">
        { selectedCategory == 0 && (
          <Grid container spacing={1}>
              { presets.map(preset => <PresetItem key={ preset._id } preset={ preset } 
              base={findCollectionElementByName(bases, preset.base)}
              switc={findCollectionElementByName(switches, preset.switch)}
              keycap={findCollectionElementByName(keycaps, preset.keycaps)}
              color={findCollectionElementByName(colors, preset.color)}/>) }
          </Grid>
        )}
        { selectedCategory == 1 && (
            <Grid container spacing={1}>
                { bases.map(base => <BaseItem key={ base._id } base={ base }/>) }
            </Grid>
        )}
        { selectedCategory == 2 && (
            <Grid container spacing={1}>
                { switches.map(switc => <SwitchItem key={ switc._id } switc={ switc }/>) }
            </Grid>
        )}
        { selectedCategory == 3 && (
          <Grid container spacing={1}>
              { keycaps.map(keycap => <KeycapItem key={ keycap._id } keycap={ keycap }/>) }
          </Grid>
        )}
        { selectedCategory == 4 && (
          <Grid container spacing={1}>
              { colors.map(color => <ColorItem key={ color._id } color={ color }/>) }
          </Grid>
        )}
    </Container>
  );
}

export default Selector;