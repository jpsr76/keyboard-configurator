import React, { useState } from 'react';

import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

import { useConfig } from './ConfigContext';
import { Margin } from '@mui/icons-material';

const HeaderBar = () => {
  const { selectedCategory, setCategory } = useConfig();

  const handleChange = (event, newCategory) => {
    if (newCategory !== null) {
      setCategory(newCategory);
    }
  };

  return (
    <ToggleButtonGroup
      color="primary"
      value={selectedCategory}
      exclusive
      onChange={handleChange}
      aria-label="Platform"
      fullWidth
    >
      <ToggleButton value={0}>Presets</ToggleButton>
      <ToggleButton value={1}>Bases</ToggleButton>
      <ToggleButton value={2}>Switches</ToggleButton>
      <ToggleButton value={3}>Keycaps</ToggleButton>
      <ToggleButton value={4}>Colors</ToggleButton>
    </ToggleButtonGroup>
  );
}

export default HeaderBar;