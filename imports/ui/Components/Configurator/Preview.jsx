import React from 'react';

import { styled } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';

import { useConfig } from './ConfigContext';

import BaseSvgComponent from './Preview/BaseSvgComponent'
import SwitchesSvgComponent from './Preview/SwitchesSvgComponent'
import KeycapsSvgComponent from './Preview/KeycapsSvgComponent'

const Preview = () => {

  const { selectedBase, selectedSwitch, selectedKeycap, selectedColor } = useConfig();

  return (
    <Container component="main" disableGutters>
        <Box
          sx={{
            marginTop: 4,
            alignItems: 'center',
            position: 'relative',
            width: '100%',
            height: '100%',
            paddingBottom: '80%'
          }}
        >
          {(selectedBase != null) &&
          <Box position={'absolute'} width={'100%'}>
            <BaseSvgComponent width={"100%"}/>
          </Box>
          }
          {(selectedBase != null && selectedSwitch != null) &&
          <Box position={'absolute'} width={'100%'}>
            <SwitchesSvgComponent width={"100%"}/>
          </Box>
          }
          {(selectedBase != null && selectedSwitch != null && selectedKeycap != null) &&
          <Box position={'absolute'} width={'100%'}>
            <KeycapsSvgComponent width={"100%"}/>
          </Box>
          }
        </Box>
    </Container>
  );
}

export default Preview;