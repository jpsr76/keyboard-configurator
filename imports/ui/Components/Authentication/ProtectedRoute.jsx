import { Meteor } from 'meteor/meteor';
import React from 'react';

import { Navigate, useLocation } from 'react-router-dom';

import { useAuth } from './AuthContext';

export const ProtectedRoute = ({ children }) => {
    const { user } = useAuth();
    const location = useLocation();

    // Need the Meteor call here on site refresh to avoid waiting
    // for useTracker updates causing instant redirect even when logged in
    if (!user && !!!Meteor.userId()) {
      return <Navigate to="/login" replace state={{ from: location }} />;
    }
  
    return children;
  };