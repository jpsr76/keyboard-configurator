import React from 'react';
import { Meteor } from 'meteor/meteor';
import { useNavigate, useLocation } from 'react-router-dom';
import { useTracker } from 'meteor/react-meteor-data';

import { AuthContext } from './AuthContext';



const AuthProvider = ({ children }) => {
    const navigate = useNavigate();
    const location = useLocation();
  
    const user = useTracker(() => Meteor.user());

    const handleRegister = (e) => {
        if (e == null) {
            navigate('/');
        }
    };

    const handleLogin = (e) => {
        if (e == null) {
            const origin = location.state?.from?.pathname || '/';
            navigate(origin)
        }
    };

    // Design Decision: Maybe dont navigate on logout
    const handleLogout = (e) => {
        if (e == null) {
            navigate('/');
        }
    };
  
    const value = {
      user,
      onRegister: handleRegister,
      onLogin: handleLogin,
      onLogout: handleLogout,
    };
  
    return (
      <AuthContext.Provider value={value}>
        {children}
      </AuthContext.Provider>
    );
};

export default AuthProvider;