import React from 'react';

import ListItem from '@mui/material/ListItem';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';


export const Order = ({ order }) => {
  return (
    <ListItem>
        <Paper
        sx={{
            p: 2,
            margin: 'auto',
            maxWidth: 800,
            flexGrow: 1,
            backgroundColor: (theme) =>
            theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
        }}
        >
        <Grid container spacing={2}>
            <Grid item xs={12} sm container>
            <Grid item sm container direction="column" spacing={2}>
                <Grid item xs>
                <Typography gutterBottom variant="subtitle1" component="div">
                    {order.name}
                </Typography>
                { (order.description != '' && order.description != null) && (
                    <Typography variant="body2" gutterBottom>
                    {order.description}
                    </Typography>
                 )}
                <Typography variant="body2" color="text.secondary">
                    BASE: {order.base}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    SWITCHES: {order.switch}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    KEYCAPS: {order.keycaps}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    COLOR: {order.color}
                </Typography>
                </Grid>
            </Grid>
            <Grid item sm={2} container direction="column" spacing={2}>
                <Grid item xs>
                <Typography gutterBottom variant="subtitle1" component="div" align="right">
                Total: {order.price}
                </Typography>
                <Typography variant="body2" color="text.secondary" align="right">
                Date: {order.createdAt.getDate() + '-' + order.createdAt.getMonth() + '-' + order.createdAt.getFullYear()}
                </Typography>
                </Grid>
            </Grid>
            </Grid>
        </Grid>
        </Paper>
    </ListItem>
  );
};