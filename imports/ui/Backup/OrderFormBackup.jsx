import React, { useState } from 'react';
import { OrdersCollection } from '/imports/api/OrdersCollection';

export const OrderForm = () => {
  const [text, setText] = useState('');

  const handleSubmit = e => {
    e.preventDefault();

    if (!text) return;

    OrdersCollection.insert({
      text: text.trim(),
      createdAt: new Date()
    });

    setText('');
  };

  return (
    <form className="order-form" onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Type to add new orders"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />

      <button type="submit">Add Order</button>
    </form>
  );
};