import React from 'react';
import HeaderBar from './ConfigHeaderBar';
import Selector from './Selector';
import Preview from './Preview';
import Summary from './Summary';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    //color: theme.palette.text.secondary,
  }));

const ConfigLayout = () => {
  return (
    <>
    <div>
        <HeaderBar />
    </div>
    <div>
        <Selector />
        <Preview />
        <Summary />
    </div>
    </>
  );
}

export default ConfigLayout;