return (
    <Box sx={{ flexGrow: 1,}}>
      <Grid container columnSpacing={2} rowSpacing={2} columns={24}>
        <Grid item xs={24}>
            <HeaderBar />
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={6}>
            <Selector />
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={8}>
            <Preview />
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={6}>
            <Summary />
        </Grid>
        <Grid item xs={1} />
      </Grid>
    </Box>
  );