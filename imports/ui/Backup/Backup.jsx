import React from 'react';


export const Backup = () => {



return (
  <div id={"Site"}>
   <div id={"Leftside"}>
    <div id={"Kategorien"}>
        <div className={"kat"}>
          Base
        </div>
        <div className={"kat"}>
          Switches
        </div>
        <div className={"kat"}>
          Keycaps
        </div>
  </div>
  <div id={"Optionen"}>
      <div className={"opt"}>
          Schwarzes PVC
      </div >
      <div className={"opt"}>
          Akazienholz
      </div>
      <div className={"opt"}>
          Gebürstetes Aluminium
      </div>
    </div>
  </div>
  <div id={"Zusammenfassung"}>
      <div id={"zsm-top"}>
            <button id={"Verlauf"}>
                Bestellverlauf
            </button>
      </div>
      <div id={"zsm-middle"}>
            <p id={"zsm-text"}>
                Bestellungs-Konfiguration:
            </p>
      </div>
      <div id={"zsm-bottom"}>
            <button id={"Bestellen"}>
                Bestellen
            </button>
      </div>
  </div>
  </div>
);
};
