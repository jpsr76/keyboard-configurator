// Components/NavBar.js
import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
 return (
 <nav>
       <ul>
          <li>
             <Link to="/">Configurator</Link>
          </li>
          <li>
             <Link to="/login">Login</Link>
          </li>
          <li>
             <Link to="/register">Register</Link>
          </li>
          <li>
             <Link to="/checkout">Checkout</Link>
          </li>
          <li>
             <Link to="/history">History</Link>
          </li>
          <li>
             <Link to="/about">About</Link>
          </li>
          <li>
             <Link to="/admin">Admin</Link>
          </li>
       </ul>
 </nav>
 );
};

export default NavBar;