import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';

import BlockIcon from '@mui/icons-material/Block';

import { SwitchesCollection } from '/imports/api/SwitchesCollection';
import ButtonBase from '@mui/material/ButtonBase';

import { useConfig } from '../ConfigContext';

export const GridSwitchItems = () => {

    const { setSwitch } = useConfig();

    const switches = useTracker(() =>
    SwitchesCollection.find({}).fetch()
    );

    const handleElementClick = (switc) => {
        setSwitch(switc);
      }

    const SwitchItem = ({ switc }) => {

        return (
            <Grid item xs={6}>
                <ButtonBase onClick={(() => handleElementClick(switc))} sx={{ width: '100%', height: '100%' }}>
                    <Paper
                    sx={{
                        p: 2,
                        margin: 'auto',
                        flexGrow: 1,
                        backgroundColor: (theme) =>
                        theme.palette.mode === 'dark' ? '#1A2027' : '#fCfCfC',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        height: '100%'
                    }}
                    >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <BlockIcon fontSize='large' />
                    </Avatar>
                    <Typography component="h1" variant="h6" align='center'>
                        {switc.name} 
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Price: ${switc.price}
                    </Typography>
                    </Paper>
                </ButtonBase>
            </Grid>
        );
    }
    
    return (
        <Grid container spacing={1}>
            { switches.map(switc => <SwitchItem key={ switc._id } switc={ switc }/>) }
        </Grid>
    );
};