import { Meteor } from 'meteor/meteor';
import React, { useState, Fragment } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { OrdersCollection } from '/imports/api/OrdersCollection';
import { Order } from './OrderBackup';
import { OrderForm } from './OrderFormBackup';
import { Login } from '../Pages/Login';

export const HistoryBackup = () => {
    const user = useTracker(() => Meteor.user());
    const userId = useTracker(() => Meteor.userId());

    const userFilter = user ? { userId: user._id } : {};

    const orders = useTracker(() => {
        if (!user) {
            return [];
        }

        return OrdersCollection.find({}, { sort: { createdAt: -1 } }).fetch();
        });

    const logout = () => Meteor.logout();
    
return (
    <div id={"Verlauf"}>
        <div>
            <h1>
                Bestellverlauf von
            </h1>
        </div>

        <div className="main">
        {user ? (
          <Fragment>
            <div className="user" onClick={logout}>
              {user.username} 🚪
            </div>

            <OrderForm user={user} />

            <div id={"Liste"}>
            <ul>
                { orders.map(order => <Order key={ order._id } order={ order }/>) }
            </ul>
            </div>
            <div className={"buttonbox"}>
                <button>Konfigurator</button>
            </div>
          </Fragment>
        ) : (
          <Login />
        )}
        </div>
    </div>
  );
};