import React from 'react';

import { Routes, Route } from 'react-router-dom';

import { Configurator } from "./Pages/Configurator";
import { Login } from "./Pages/Login";
import { Register } from "./Pages/Register";
import  Checkout  from "./Pages/Checkout";
import { History } from "./Pages/History";
import { About } from "./Pages/About";
import { Admin } from "./Pages/Admin";
import { NoPage } from "./Pages/NoPage";

import AuthProvider from './Components/Authentication/AuthProvider';
import { ProtectedRoute } from './Components/Authentication/ProtectedRoute';

import BasicLayout from './Components/General/BasicLayout';

export const App = () => {

    return (
        <>
        <AuthProvider>
            <BasicLayout>
                <Routes>
                    <Route path="/" element={<Configurator />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/checkout" element={<Checkout />} />
                    <Route path="/history" element={
                        <ProtectedRoute>
                            <History />
                        </ProtectedRoute>
                    } />
                    <Route path="/about" element={<About />} />
                    <Route path="/admin" element={
                        <ProtectedRoute>
                            <Admin />
                        </ProtectedRoute>
                    } />
                    <Route path="*" element={<NoPage />} />
                </Routes>
            </BasicLayout>
        </AuthProvider>
        </>
    );
};