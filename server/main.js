import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import { BasesCollection } from '/imports/api/BasesCollection';
import { ColorsCollection } from '/imports/api/ColorsCollection';
import { KeycapsCollection } from '/imports/api/KeycapsCollection';
import { SwitchesCollection } from '/imports/api/SwitchesCollection';
import { OrdersCollection } from '/imports/api/OrdersCollection';
import { PresetsCollection } from '../imports/api/PresetsCollection';
import { DiscountCodesCollection } from '../imports/api/DiscountCodesCollection';



const SEED_USERNAME = 'admin';
const SEED_PASSWORD = 'password';

export const insertPreset = (presetName, presetDescription, presetBase, presetSwitch, presetKeycap, presetColor, presetIcon, presetImage) =>
PresetsCollection.insert({
    name: presetName,
    description: presetDescription,
    base: presetBase,
    switch: presetSwitch,
    keycaps: presetKeycap,
    color: presetColor,
    icon: presetIcon,
    image: presetImage,
    createdAt: new Date()
  });

export const insertBase = (baseName, basePrice, baseDescription, baseIcon, baseImage, baseSVG) =>
BasesCollection.insert({
    name: baseName,
    price: basePrice,
    description: baseDescription,
    icon: baseIcon,
    image: baseImage,
    svg: baseSVG,
    createdAt: new Date()
  });

export const insertSwitch = (switchName, switchPrice, switchDescription, switchIcon, switchImage, switchSVG) =>
SwitchesCollection.insert({
    name: switchName,
    price: switchPrice,
    description: switchDescription,
    icon: switchIcon,
    image: switchImage,
    svg: switchSVG,
    createdAt: new Date()
  });

export const insertKeycap = (keycapName, keycapPrice, keycapDescription, keycapIcon, keycapImage, keycapSVG) =>
KeycapsCollection.insert({
    name: keycapName,
    price: keycapPrice,
    description: keycapDescription,
    icon: keycapIcon,
    image: keycapImage,
    svg: keycapSVG,
    createdAt: new Date()
  });

export const insertColor = (colorName, colorPrice, colorDescription, colorIcon, colorImage, colorSVG) =>
ColorsCollection.insert({
    name: colorName,
    price: colorPrice,
    description: colorDescription,
    icon: colorIcon,
    image: colorImage,
    svg: colorSVG,
    createdAt: new Date()
  });

export const insertOrder = (
  orderName, orderDescription, orderIsPreset,
  orderBase, orderSwitch, orderKeycap,
  orderColor, orderDiscount, orderPrice, user) =>
OrdersCollection.insert({
    name: orderName,
    description: orderDescription,
    isPreset: orderIsPreset,
    base: orderBase,
    switch: orderSwitch,
    keycaps: orderKeycap,
    color: orderColor,
    discount: orderDiscount,
    price: orderPrice,
    userId: user._id,
    createdAt: new Date(),
  });

  export const insertDiscountCode = (discountName, discountPercent) =>
  DiscountCodesCollection.insert({
    name: discountName,
    percent: discountPercent,
    createdAt: new Date()
  });
  

  // Creating Default Values to initialize Database
Meteor.startup(() => {
  if (!Accounts.findUserByUsername(SEED_USERNAME)) {
    Accounts.createUser({
      username: SEED_USERNAME,
      password: SEED_PASSWORD,
    });
  }

  const user = Accounts.findUserByUsername(SEED_USERNAME);

  if (PresetsCollection.find().count() === 0) {

    insertPreset('Basic', 'Unser goldener Standard', 'Schwarzes PVC', 'Cherry MX Red', 'Standard', 'Rot', 'presetIcon', 'presetImage');
    insertPreset('Gaming', 'Preset optimisiert für Gaming-Performance', 'Gebürstetes Aluminium', 'Cherry MX Speed', 'Standard', 'Schwarz und Rot', 'presetIcon', 'presetImage');
  }

  if (BasesCollection.find().count() === 0) {

    insertBase('Schwarzes PVC', 60, 'Einfaches schwarzes PVC', 'baseIcon', 'baseImage', 'baseSVG');
    insertBase('Akazienholz', 140, 'Echtes Akazienholz, gepflegt und geölt', 'baseIcon', 'baseImage', 'baseSVG');
    insertBase('Gebürstetes Aluminium', 100, 'Leichtes und robustes Aluminium, gebürstet', 'baseIcon', 'baseImage', 'baseSVG');
    insertBase('Stainless Steel', 100, 'Solider rostfreier Edelstahl', 'baseIcon', 'baseImage', 'baseSVG');
  }

  if (SwitchesCollection.find().count() === 0) {

    insertSwitch('Cherry MX Red', 20, 'Taktil mit geringem Widerstand', 'switchIcon', 'switchImage', 'switchSVG');
    insertSwitch('Cherry MX Blue', 20, 'Taktil mit audiblem Klick-Feedback', 'switchIcon', 'switchImage', 'switchSVG');
    insertSwitch('Cherry MX Brown', 20, 'Taktil aber leise', 'switchIcon', 'switchImage', 'switchSVG');
    insertSwitch('Cherry MX Speed', 30, 'Extrem schnelle Aktuation', 'switchIcon', 'switchImage', 'switchSVG');
  }

  if (KeycapsCollection.find().count() === 0) {

    insertKeycap('Standard', 30, 'Our Standard Keycaps', 'keycapIcon', 'keycapImage', 'keycapSVG');
    insertKeycap('Flat', 20, 'Flat-top Keycaps', 'keycapIcon', 'keycapImage', 'keycapSVG');
    insertKeycap('Concave', 25, 'Concave Keycaps', 'keycapIcon', 'keycapImage', 'keycapSVG');
    insertKeycap('Low-Profile', 25, 'Keycaps with less vertical height', 'keycapIcon', 'keycapImage', 'keycapSVG');
  }

  if (ColorsCollection.find().count() === 0) {

    insertColor('Schwarz', 0, 'Einfaches Schwarz', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Weiß', 0, 'Einfaches Weiß', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Hellgrau', 0, 'Einfaches Hellgrau', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Dunkelgrau', 0, 'Einfaches Dunkelgrau', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Rot', 0, 'Einfaches Rot', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Grün', 0, 'Einfaches Grün', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Blau', 0, 'Einfaches Blau', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Gelb', 0, 'Einfaches Gelb', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Orange', 0, 'Einfaches Orange', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Violett', 0, 'Einfaches Violett', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Schwarz und Rot', 5, 'Eine schlichte Kombination aus Schwarz und Rot', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Weiß und Blau', 5, 'Eine schlichte Kombination aus Weiß und Blau', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Metallic Black', 10, 'Schwarz in metallic optik', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Metallic Red', 10, 'Rot in metallic optik', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Silber', 10, 'Silber in metallic optik', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Gold', 10, 'Gold in metallic optik', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Perlgold', 15, 'Edles Perlgold', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Perlmutt', 15, 'Edles Perlmutt', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Galaxy', 20, 'Dunkles Lila-blau geschmückt mit Schlieren und weißen Akzenten', 'colorIcon', 'colorImage', 'colorSVG');
    insertColor('Bambus', 20, 'Hölzernes Design in Bambus-Optik', 'colorIcon', 'colorImage', 'colorSVG');
  }

  if (OrdersCollection.find().count() === 0) {
    insertOrder(
      'Special Order', 'orderDescription', true,
      'Schwarzes PVC', 'Cherry MX Red', 'Standard',
      'Perlgold', '0%', '170', user);
    insertOrder(
      'Special Order 2', 'test', true,
      'Schwarzes PVC', 'Cherry MX Red', 'Standard',
      'Gold', '0%', '150', user);
    insertOrder(
      'Special Order 3', '', true,
      'Schwarzes PVC', 'Cherry MX Red', 'Standard',
      'Gold', '0%', '150', user);
  }

  
  if (DiscountCodesCollection.find().count() === 0) {

    insertDiscountCode('Discount10', 10);
    insertDiscountCode('Discount25', 25);
  }

});
