# Keyboard Configurator



## Installation von Meteor 1/2

Meteor benötigt eine sehr bestimmte Node.js Version (>= 10 und <= 14) um zu funktionieren.
Man kann diese entweder manuell installieren oder man verwendet einen Versionsmanager, wir empfehlen Volta.

## Installation und Setup von Volta

Für die Volta Installation am besten das Angebrachte Betriebssystem auf der [Volta Seite](https://docs.volta.sh/guide/getting-started) auswählen.
Den Installer für Windows gibt es [hier](https://github.com/volta-cli/volta/releases/download/v1.1.1/volta-1.1.1-windows-x86_64.msi).

Sobald Volta installiert ist kann man die Node.js Version die man möchte installieren.


```
volta install node@14
```

## Installation von Meteor 1/2

Sobald eine zulässige Version von Node.js installiert ist kann man mit der Installation von Meteor fortfahren.

```
npm install -g meteor
```

## Projekt Setup

Zuerst navigiert man in das Verzeichnis in dem man das Projekt installieren möchte. 

Man benötigt ein Terminal was sich in diesem Verzeichnis befindet für Meteor.

Daraufhin wird das Gitlab Repository in dieses Verzeichnis geclont. Wir empfehlen gitbash mit dem Befehl:

```
git clone https://git.thm.de/jpsr76/keyboard-configurator.git
```

Nach dem clonen muss das Project noch als Meteor Projekt initialisiert werden. Dies geschieht in dem Meteor Terminal mit dem Befehl:

```
meteor create keyboard-configurator --prototype
```

Um die installation zu finalisieren muss man in das Verzeichnis navigieren und die Dependencies installieren.

```
cd .\keyboard-configurator\
meteor add accounts-password
meteor npm install
```

Danach sollte das Projekt startbereit sein.


## Projekt Start

Wenn die installation erfolgreich war sollte der Start sehr einfach sein.
Dafür muss im Projektverzeichnis nur ein Befehl eingegeben werden:

```
meteor
```

Danach sollte das Projekt über [http://localhost:3000/](http://localhost:3000/) erreichbar sein.
Vorallem beim ersten Start und auf nicht so Leistungsstarken Geräten kann es sein das dieser Befehl lange braucht bzw. crasht. Wir haben keinen Weg gefunden dies zu vermeiden, da es am Meteorextractor liegt. Auf meinem Laptop hat dies bei Tests schon ~10 Minuten bei 100% CPU Auslastung gebraucht.


## Projekt Seiten

Es sind nicht alle Seiten über Verlinkungen auf der Webseite verfügbar, da diese für Erweiterungen bestimmt sind. Aktuell nicht erreichbar sind
- http://localhost:3000/admin
- http://localhost:3000/checkout
- http://localhost:3000/nopage

## MongoDB

Meteor sollte eigentlich ein eingebautes Commandline Tool haben, um auf die Datenbank zugreifen zu können.
Dies funktioniert gar nicht mehr. Darum empfehlen wir ein eigenes Tool zu installieren, wenn man auf die Datenbank zugreifen möchte.

- [MongoSH als CLI](https://www.mongodb.com/docs/mongodb-shell/install/#std-label-mdb-shell-install)
- [NoSQLBooster für MongoDB](https://nosqlbooster.com/downloads)

Wenn das Projekt gestartet ist, ist die Datenbank über [http://localhost:3001/](http://localhost:3001/) erreichbar